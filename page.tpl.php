<?php

/**
 * @file page.tpl.php
 * Theme implementation to display a single Drupal page.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <!--[if IE]>
    <link rel="stylesheet" href="<?php print $base_path . $directory; ?>/ie.css" type="text/css">
  <![endif]-->
  <?php print $scripts; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
</head>
<?php
/**
 * Change the body id selector to your preferred layout.
 * E.g body id="genesis_5", LITE uses genesis_1 by default but supports all layouts.
 *
 * @see layout.css
 */ 
?>
<body id="genesis_1" <?php print $page_classes; ?>>
  <div id="container" class="width <?php print $body_classes; ?> <?php // print 'grid' ;?>">

    <?php if (!empty($leaderboard)): ?>
		    <div id="leaderboard" class="region">
			     <?php print $leaderboard; ?>
		    </div>
    <?php endif; ?>

    <div id="header" class="clear-block">

       <div id="branding">

										<?php if (!empty($site_logo)): ?>
												<div id="logo"><?php print $site_logo; ?></div>
										<?php endif; ?>

										<?php if (!empty($site_name)): print $site_name; endif; ?>

										<?php if (!empty($site_slogan)): ?>
												<div id="site-slogan"><?php print $site_slogan; ?></div>
										<?php endif; ?>

      </div> <!-- /branding -->

						<?php if (!empty($search_box)): ?>
								<?php print $search_box; ?>
						<?php endif; ?> <!-- /search-box -->

						<?php if (!empty($header)): ?>
								<div id="header-blocks">
										<?php print $header; ?>
								</div>
						<?php endif; ?>

    </div>  <!-- /header -->

				<?php if ($primary_menu || $secondary_menu): ?>
						<div id="nav" class="menu <?php print $nav_class; ?>">
						
										<?php if (!empty($primary_menu)): ?>
												<div id="primary" class="clear-block">
														<?php print $primary_menu; ?>
												</div>
										<?php endif; ?>

										<?php if (!empty($secondary_menu)): ?>
												<div id="secondary" class="clear-block">
														<?php print $secondary_menu; ?>
												</div>
										<?php endif; ?>

						</div> <!-- /navigation -->
				<?php endif; ?>

				<?php if (!empty($breadcrumb)): ?><?php print $breadcrumb; ?><?php endif; ?>

				<?php if ($secondary_content): ?>
						<div id="secondary-content" class="region clear">
								<?php print $secondary_content; ?>
						</div> <!-- /secondary-content -->
				<?php endif; ?>

				<div id="columns" class="clear clear-block">
						<div id="content">
								<div id="content-inner">

										<?php if (!empty($mission)): print $mission; endif; ?>

										<?php if ($content_top): ?>
												<div id="content-top" class="region">
														<?php print $content_top; ?>
												</div> <!-- /content_top -->
										<?php endif; ?>

										<div id="main-content">
												<?php if (!empty($title)): ?>
												  <h1 id="page-title"><?php print $title; ?></h1>
												<?php endif; ?>
												<?php if (!empty($tabs)): ?>
												  <div class="tabs"><?php print $tabs; ?></div>
												<?php endif; ?>
												<?php if (!empty($messages)): print $messages; endif; ?>
												<?php if (!empty($help)): print $help; endif; ?>
												<?php print $content; ?>
										</div>

										<?php if ($content_bottom): ?>
												<div id="content-bottom" class="region">
														<?php print $content_bottom; ?>
												</div> <!-- /content_bottom -->
										<?php endif; ?>

										<?php if ($feed_icons): ?><?php print $feed_icons; ?><?php endif; ?>

								</div>
						</div>

						<?php if (!empty($left)): ?>
								<div id="sidebar-left" class="sidebar">
										<?php print $left; ?>
								</div> <!-- /sidebar-left -->
						<?php endif; ?>

						<?php if (!empty($right)): ?>
								<div id="sidebar-right" class="sidebar">
										<?php print $right; ?>
								</div> <!-- /sidebar-right -->
						<?php endif; ?>

				</div> <!-- /col wrapper -->

				<?php if ($tertiary_content): ?>
						<div id="tertiary-content" class="clear">
								<?php print $tertiary_content; ?>
						</div> <!-- /tertiary_content -->
				<?php endif; ?>

				<div id="footer" class="region clear">
						<?php if (!empty($footer)): print $footer; endif; ?>
						<?php if (!empty($footer_message)): print $footer_message; endif; ?>
				</div> <!-- /footter -->

  </div> <!-- /container -->
		
		<?php print $closure ?>
		
</body>
</html>