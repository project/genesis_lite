<?php 

/**
 * @file node.tpl.php
 * Theme implementation to display a node.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 */
?>
<div class="node <?php print $node_classes; ?>" id="node-<?php print $node->nid; ?>">

		<?php if ($page == 0): ?>
			 <h2 class="node-title"><a href="<?php print $node_url; ?>" rel="bookmark"><?php print $title; ?></a></h2>
		<?php endif; ?>

		<?php if ($unpublished): ?>
			 <div class="unpublished"><?php print t('Unpublished'); ?></div>
		<?php endif; ?>

		<?php if (!empty($picture)): ?><?php print $picture; ?><?php endif; ?>

		<?php if ($submitted): ?>
			 <div class="submitted">
			   <?php print $submitted; ?>
			 </div>
		<?php endif; ?>

		<?php print $content; ?>
		
		<?php if (count($taxonomy)): ?><?php print $terms; ?><?php endif; ?>

		<?php if ($links): ?><?php print $links; ?><?php endif; ?>

</div> <!-- /node -->