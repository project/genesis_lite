<?php 

/**
 * @file block.tpl.php
	*
 * Theme implementation to display a block.
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 */
?>
<div id="block-<?php print $block->module .'-'. $block->delta; ?>" class="block <?php print $block_classes; ?>">

		<?php if ($block->subject): ?>
			 <h2 class="block-title"><?php print $block->subject; ?></h2>
		<?php endif; ?>
	
		<?php print $block->content ?>
	
		<?php print $edit_links; ?>
		
</div>